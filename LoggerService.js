const winston = require('winston');
const format = winston.format;
const callingModule = require.main;
require('dotenv').config()
const env = process.env.LOG_ENV;


const parseError = function(e){
    const stack = e.stack;
    const message = e.message;
    const caller_line = stack.split('\n')[1];
    const source = caller_line.substring(caller_line.indexOf('(')+1,caller_line.indexOf(')'));
    return {
        errorMessage : message,
        errorSource : source
    }
}

loggerService = {};
loggerService.createLogger = function(config){
    
    logger = {};

    logger.isActive = config.isActive ? true : false;
    logger.levels = config.levels ? config.levels : {};
    
    if(!config.logSource) logger.logSource = false;
    else logger.logSource = config.logSource;

    if(config.fileMode === 'delete'){
        logger.fileMode = 'w';
    }
    else{
        logger.fileMode = 'a';
    }

    if(config.format){
        if(config.format === 'simple'){
            logger.format = format.simple();
        }
        else if(config.format === 'json'){
            logger.format = format.json();
        }
        else{
            logger.format = config.format;
        }
    }
    else{
        logger.format = format.simple();
    }

    var transports = [];
    for(var level in logger.levels){
        var levelData = logger.levels[level];
        if(levelData.destinations){
            // console.log(levelData.destinations);
            for(var i in levelData.destinations){
                var filename = levelData.destinations[i];
                // console.log(filename);
                transports.push(new winston.transports.File({
                        label : callingModule.filename,
                        filename : filename,
                        options: { flags: logger.fileMode},
                        level : level,
                    }));
                // console.log(level,filename);
            }
        }
        else{
            transports.push(new winston.transports.Console({
                label : callingModule.filename,
                level : level, 
            }));
        }
    }

    logger.server = winston.createLogger({
        transports : transports,
        format : format.combine(
            format.timestamp(),
            logger.format
        )
    });



    /**logger Methods*/
    logger.isLevelValid = function(level){
        if(!logger.levels[level]){
            return false;
        }
        var levelData = logger.levels[level];
        if(!levelData.modes){
            return true;
        }
        // console.log(levelData.modes, process.env.NODE_ENV, levelData.modes.includes(process.env.NODE_ENV));
        return levelData.modes.includes(env);
    }

    /**
     * @param {message,level} data 
     * message : required
     * level : optional - default value - info
     */
    logger.log = function(message,level,params){

        if(!level){
            level = 'info';
        }
        "error.log"
        if(logger.isLevelValid(level)){
            var params = params ? params : {};
            if(logger.logSource){
                params.source = callingModule.filename;
            }
            logger.server.log(level,message,params);
        }
        else{
            // console.log("Logging not possible!");
        }        

    }

    logger.info = function(message,params){
        logger.log(message,"info",params);
    }

    logger.error = function(message, e){
        if(message instanceof Error){
            logger.log('',"error",{error: parseError(message)});
        }
        else if(e instanceof Error){
            logger.log(message, "error", {error: parseError(e)});
        }
        else{
            logger.log(message,"error",e);
        }
    }

    logger.warn = function(message,params){
        logger.log(message,"warn",params);
    }

    logger.debug = function(message,params){
        logger.log(message,"debug",params);
    }

    logger.verbose = function(message,params){
        logger.log(message,"verbose",params);
    }

    logger.http = function(message,params){
        logger.log(message,"http",params);
    }

    logger.silly = function(message,params){
        logger.log(message,"silly",params);
    }

    return logger;
}

module.exports = loggerService;
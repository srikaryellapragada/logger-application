// /*
// * An example of custom format
// */
const winston = require('winston');
var format = winston.format.printf((info) => {
//    var message = `${new Date().toString()} | ${info.level.toUpperCase()} | ${info.message} | `;
//    message = info.source ? message + `${info.source} | ` : message;
//    message = info.error ? message + `${info.error.errorMessage} | ` + `${info.error.errorSource}` : message;
//    return message

    var message = `${new Date().toString()} | ${info.level.toUpperCase()} | ${info.message}`;
    if(info.error){
        if(info.message.length>0){
            message = message + ' ';
        }
        message = message + `${info.error.errorMessage}`;
        message = message + ` | ${info.error.errorSource};`
    }
    else{
        message = info.source ? message + ` | ${info.source}` : message;
    }
    return message;
});

module.exports = {

    "isActive" : true,              // optional, default value : true

    "levels" : {

        "error" : {
            "modes" : [ "local" , "production"],
            "destinations" : ["error.log"]
        },

        "info" : {
            "modes" : [ "local" , "production"],    //optional, default value : all modes
            "destinations" : ["info.log"]           //optional, default value : console
        },

        "warn" : {
            "modes" : ["local"],
            "destinations" : ["warn.log"]
        },
    },

    "fileMode" : "delete",   //options: delete, update - delete deletes the old logs. update appends to the old logs. default value : update. 

    "format" : format,       //options: simple, json, or can be custom format object of winston library. default value : simple

    "logSource"  : true        // optional, default value: false

}
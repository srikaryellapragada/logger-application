const LoggerService = require('./LoggerService');
const loggerConfig = require('./config');

var logger = LoggerService.createLogger(loggerConfig);

logger.info('INFO_TEST');
logger.error('ERROR_TEST',new Error('This is error 1'));
logger.error(new Error("This is error 2"));
logger.error("error test 3");